﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookCenter : MonoBehaviour {

	// Use this for initialization
	void Start () {
#if UNITY_STANDALONE_WIN || UNITY_ANDROID
        this.transform.LookAt(new Vector3(0, 0, 5));
#endif
    }

    // Update is called once per frame
    void Update () {
		
	}
}
