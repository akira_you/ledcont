﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Video;

public class VideoToLed : MonoBehaviour
{
    public RenderTexture tex;
    public VideoPlayer player;
    public List<VideoClip> clip;
    Texture2D buf;
    void Start()
    {
        buf = new Texture2D(
             tex.width, tex.height, TextureFormat.ARGB32, false);
    }
    bool prepared = false;
    IEnumerator SeekFirst()
    {
        while (player.frame < 5)
        {
            yield return null;
        }
        player.Pause();
        prepared = true;
        yield break;
    }
    public void VideoPrepare(int i)
    {
        prepared = false;
        player.clip = clip[i];
        player.frame = 0;
        player.waitForFirstFrame = true;
        player.Play();
        StartCoroutine(SeekFirst());
    }
    public void SetLoop(bool t)
    {
        player.isLooping = t;
    }
    public bool IsPlaying()
    {
        return player.isPlaying;
    }
    public void VideoPlay()
    {
        player.Play();
    }
    public void VideoPause()
    {
        player.Pause();
    }
    public bool IsReady()
    {
        return prepared;
    }
    public float SetLed(List<Led> leds)
    {
        if (!player.isPlaying)
        {
            foreach (Led led in leds)
            {
                led.SetColor(new Color32());
            }
            return 0;

        }



        RenderTexture.active = tex;
        buf.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
        foreach(Led led in leds)
        {
            int x = (int)( (led.relPos.x +1)/2 * tex.width);
            int y = (int)( (led.relPos.y +1)/2 * tex.height);
            led.SetColor(buf.GetPixel(x, y));
        }
        return (float)player.frame / player.frameCount;

    }
    // Update is called once per frame
    void Update()
    {
    }
}
