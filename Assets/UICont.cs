﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UICont : MonoBehaviour {
    public FlickGesture flickGesture;
    public Setting setting;
    UdpClient udp;

 
    // Use this for initialization
    void Start () {
        udp = new UdpClient(13532);
        udp.Client.ReceiveTimeout = 1;
    }
	
	// Update is called once per frame
	void Update () {
        if (udp.Available > 0)
        {
            IPEndPoint remoteEP = null;
            byte[] data = udp.Receive(ref remoteEP);
            string text = Encoding.ASCII.GetString(data);
            switch (text)
            {
                case "start":
                    setting.KickStart();
                    break;
                case "reset":
                    setting.KickReset();
                    break;
                default:
                    Debug.Log("unknown command:" + text);
                    break;
            }
        }

	}

    private void OnEnable()
    {
        flickGesture.Flicked += OnFlicked;

    }
    private void OnDisable()
    {
        flickGesture.Flicked -= OnFlicked;

    }
    void OnFlicked(object Sender,System.EventArgs e)
    {
        setting.KickStart();
    }
}
